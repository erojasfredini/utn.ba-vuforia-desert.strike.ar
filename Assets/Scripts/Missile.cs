﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Missile : MonoBehaviour
{
    public float maxTiempo = 2.0f;
    public float velocidad = 1.0f;
    public GameObject explosionVFX;
    private Rigidbody _rb;
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        Invoke("Explode", maxTiempo);
    }

    void FixedUpdate()
    {
        _rb.velocity = transform.forward * velocidad;
    }

    private void OnCollisionEnter(Collision collision)
    {
        int layerPiso = 1 << LayerMask.NameToLayer("Ground");
        if (collision.gameObject.layer != layerPiso)
        {
            Explode();
        }
    }

    private void Explode()
    {
        GameObject.Destroy(gameObject);
        if (explosionVFX != null)
        {
            GameObject.Instantiate(explosionVFX, transform.position, Quaternion.Euler(Random.onUnitSphere));
        }
    }
}