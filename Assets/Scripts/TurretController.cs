﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using UnityEngine;

public class TurretController : MonoBehaviour
{
    public Transform torre;
    public Transform torreOrigenMisil;
    public float velocidadRotacion = 1.0f;
    public float cadenciaDisparo = 1.0f;
    public GameObject misilPrefab;
    private Transform _ship = null;

    void Start()
    {
        var shipObj = GameObject.FindGameObjectWithTag("Player");
        if (shipObj)
        {
            _ship = shipObj.transform;
        }
        InvokeRepeating("Fire", cadenciaDisparo, cadenciaDisparo);
    }

    void Update()
    {
        var dir = (_ship.position - transform.position).normalized;
        var targetRot = Quaternion.LookRotation(dir, transform.up);
        torre.rotation = Quaternion.Slerp(torre.rotation, targetRot, Time.deltaTime * velocidadRotacion);
    }

    private void Fire()
    {
        var dir = (_ship.position - transform.position).normalized;
        var targetRot = Quaternion.LookRotation(dir);
        GameObject.Instantiate(misilPrefab, torreOrigenMisil.position, targetRot, transform.parent);
    }
}
