﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class ShipController : MonoBehaviour
{
    private Camera _cam = null;
    private NavMeshAgent _agent;
    [HideInInspector]
    public Vector3 target = Vector3.zero;
    private ParticleSystem[] _particles;
    private int _layerPiso;

    void Start()
    {
        _cam = Camera.main;
        _agent = GetComponent<NavMeshAgent>();
        target = transform.position;
        _particles = GetComponentsInChildren<ParticleSystem>();
        _layerPiso = 1 << LayerMask.NameToLayer("Ground");
    }

    void Update()
    {
        UpdateTargetPicking();
        _agent.SetDestination(target);
        // Partciculas
        if (_agent.velocity.sqrMagnitude > Mathf.Epsilon)
        {
            foreach (var fx in _particles)
            {
                if (!fx.isPlaying)
                {
                    fx.Play();
                }
            }
        }
        else
        {
            foreach (var fx in _particles)
            {
                if (fx.isPlaying)
                {
                    fx.Stop();
                }
            }
        }
    }

    private void UpdateTargetPicking()
    {
#if UNITY_EDITOR
        if (!Input.GetMouseButton(0))
        {
            return;
        }
        var pos2D = Input.mousePosition;
        var ray = _cam.ScreenPointToRay(new Vector3(pos2D[0], pos2D[1], 0.0f));
        if (Physics.Raycast(ray, out RaycastHit hit, _layerPiso))
        {
            target = hit.point;
        }
#else
        if (Input.touchCount == 0)
        {
            return;
        }

        for (int idx = 0; idx < Input.touchCount; ++idx)
        {
            var touch = Input.GetTouch(idx);
            if ((touch.phase == TouchPhase.Began) || (touch.phase == TouchPhase.Moved))
            {
                var pos2D = touch.position;
                var ray = _cam.ScreenPointToRay(new Vector3(pos2D[0], pos2D[1], 0.0f));
                if (Physics.Raycast(ray, out RaycastHit hit, _layerPiso))
                {
                    target = hit.point;
                    break;
                }
            }
        }
#endif
    }
}
