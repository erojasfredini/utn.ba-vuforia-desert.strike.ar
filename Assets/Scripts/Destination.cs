﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using UnityEngine;

public class Destination : MonoBehaviour
{
    public Transform markerObjetivo;
    public float ancho = 0.02f;
    private ShipController _ship = null;
    private LineRenderer _line = null;

    void Start()
    {
        var shipObj = GameObject.FindGameObjectWithTag("Player");
        if (shipObj)
        {
            _ship = shipObj.GetComponent<ShipController>();
        }
        _line = GetComponent<LineRenderer>();
        _line.startWidth = ancho;
        _line.endWidth = ancho;
    }

    void Update()
    {
		// Objetivo de nave
        if (_ship)
        {
            _ship.target = transform.position;

			// Linea
			_line.SetPosition(0, transform.parent.position);
			var gameZonePlane = new Plane(_ship.transform.parent.up, _ship.transform.parent.position);
			var proyOnPlane = gameZonePlane.ClosestPointOnPlane(transform.parent.position);
			_line.SetPosition(1, proyOnPlane);
			// Punto final de linea
			markerObjetivo.position = proyOnPlane;
			markerObjetivo.rotation.SetLookRotation(transform.parent.up);
		}
    }
}
