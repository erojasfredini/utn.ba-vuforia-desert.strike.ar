﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using UnityEngine;
using UnityEngine.Events;

public class DestinationTrackerEventHandler : DefaultTrackableEventHandler
{
    public UnityEvent OnFoundEvent;
    public UnityEvent OnLostEvent;

#region PROTECTED_METHODS
    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        if (mTrackableBehaviour)
        {
            if (OnFoundEvent != null)
            {
                OnFoundEvent.Invoke();
            }
            var destinationComponents = mTrackableBehaviour.GetComponentsInChildren<Destination>(true);
            var lineComponents = mTrackableBehaviour.GetComponentsInChildren<LineRenderer>(true);
            var spriteComponents = mTrackableBehaviour.GetComponentsInChildren<SpriteRenderer>(true);
            var animationComponents = mTrackableBehaviour.GetComponentsInChildren<Animation>(true);

            // Enable destination script:
            foreach (var component in destinationComponents)
                component.enabled = true;

            // Enable line renderer:
            foreach (var component in lineComponents)
                component.enabled = true;

            // Enable sprites:
            foreach (var component in spriteComponents)
                component.enabled = true;

            // Enable animations:
            foreach (var component in animationComponents)
                component.enabled = true;
        }
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();
        if (mTrackableBehaviour)
        {
            if (OnLostEvent != null)
            {
                OnLostEvent.Invoke();
            }
            var destinationComponents = mTrackableBehaviour.GetComponentsInChildren<Destination>(true);
            var lineComponents = mTrackableBehaviour.GetComponentsInChildren<LineRenderer>(true);
            var spriteComponents = mTrackableBehaviour.GetComponentsInChildren<SpriteRenderer>(true);
            var animationComponents = mTrackableBehaviour.GetComponentsInChildren<Animation>(true);

            // Disable destination script:
            foreach (var component in destinationComponents)
                component.enabled = false;

            // Disable line renderer:
            foreach (var component in lineComponents)
                component.enabled = false;

            // Enable sprites:
            foreach (var component in spriteComponents)
                component.enabled = false;

            // Enable animations:
            foreach (var component in animationComponents)
                component.enabled = false;
        }
    }
#endregion // PROTECTED_METHODS
}
