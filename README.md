# Desert Strike
Demo técnico para ver qué tipo de cosas podemos hacer con **Augmented Reality** (AG) y aprender algunas cosas nuevas :)

La aplicación permite mover a una nave que se moverá sobre un plano de juego, y además permitirá utilizar un marcador para determinar el destino. Además, se podrá agregar un torreta y un edificio a través  de otros marcadores.

## Requisitos
*  Unity 2019.2.3 o superior